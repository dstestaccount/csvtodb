<?php
/**
 * PHP script command line csv import to user table.
 * 
 * PHP Version 7.2.x
 * 
 * @category Code_Challenge
 * @package  Code_Challenge
 * @author   DS Reddy <ds.dsassociates@gmail.com>
 */
$shortopts = 'u:p:h:d:';
$longopts = ['file:', 'help', 'create_table', 'dry_run'];

$values = getopt($shortopts, $longopts);
if (isset($values['help'])) {
    fwrite(STDOUT, '* --file [csv file name] – this is the name of the CSV to be parsed');
    fwrite(STDOUT, "\n* --create_table – this will cause the MySQL users table to be built (and no further action will be taken)");
    fwrite(STDOUT, "\n* --dry_run – this will be used with the --file directive in case we want to run the script but not insert into the DB. All other functions will be executed, but the database won't be altered");
    fwrite(STDOUT, "\n* -u – MySQL username");
    fwrite(STDOUT, "\n* -p – MySQL password");
    fwrite(STDOUT, "\n* -h – MySQL host");
    fwrite(STDOUT, "\n* -d – MySQL database name");
    fwrite(STDOUT, "\n* --help – which will output the above list of directives with details.\n");
    exit;
}
if (!isset($values['dry_run']) && !isset($values['help'])) {
    if (!isset($values['u'])) {
        die("Database user missing\n");
    }
    if (!isset($values['p'])) {
        die("Database password missing\n");
    }
    if (!isset($values['h'])) {
        die("Database host missing\n");
    }
    if (!isset($values['d'])) {
        die("Database name missing\n");
    }
    // Create connection
    $conn = new mysqli($values['h'], $values['u'], $values['p'], $values['d']);
    // Check connection
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error . "\n");
    }
}
if (isset($values['create_table'])) {
    $sql = "CREATE TABLE users (
        id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
        name VARCHAR(30) NOT NULL,
        surname VARCHAR(30) NOT NULL,
        email VARCHAR(50)
        )";
        
    if ($conn->query($sql) === true) {
        die("Table users created successfully\n");
    } else {
        die("Error creating table: " . $conn->error . "\n");
    }
}
if (isset($values['file'])) {
    $file = $values['file'];
    if (!file_exists($file)) {
        die("The file $file not exist\n");
    }
    $open = fopen($values['file'], "r");
    $i = 0 ;
    $users =[];
    while (($data = fgetcsv($open, 1000, ",")) !== false) {
        if ($i > 0 && $data[0] != null) {
            $user['name'] = ucfirst($data[0]);
            $user['surnmae'] = ucfirst($data[1]);
            $user['email'] = strtolower($data[2]);
            $users[] = $user;
        }
        $i++;
    }
    fclose($open);
    if (isset($values['dry_run'])) {
        fwrite(STDOUT, "Name\t\tSurname\t\tEmail\n");
        foreach ($users as $user) {
            $name = $user['name'];
            $surname = $user['surnmae'];
            $email = $user['email'];
            fwrite(STDOUT, "$name\t\t$surname\t\t$email\n");
        }
        exit;
    }
    foreach ($users as $user) {
        $name = addslashes($user['name']);
        $surname = addslashes($user['surnmae']);
        $email = $user['email'];
        $sql = "SELECT * FROM users WHERE email='$email'";
        $result = $conn->query($sql);

        if ($result->num_rows > 0) {
            fwrite(STDOUT, "Skipped user $name - user with $email already exist\n");
            continue;
        }
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            fwrite(STDOUT, "Skipped user $name - invalid Email address\n");
            continue;
        }
        $sql = "INSERT INTO users(name, surname, email) VALUES('$name', '$surname', '$email')";
        
        $conn->query($sql);
    }
}